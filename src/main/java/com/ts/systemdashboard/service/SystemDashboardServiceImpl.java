package com.ts.systemdashboard.service;

import com.ts.systemdashboard.dao.SystemDashboardDao;
import com.ts.systemdashboard.dto.view.dashboard.ParentSegmentDto;
import com.ts.systemdashboard.dto.view.dashboard.ChildSegmentDto;
import com.ts.systemdashboard.dto.view.dashboard.DashboardDto;
import com.ts.systemdashboard.dto.view.dashboard.SiteDto;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author anoj
 */
@Service
public class SystemDashboardServiceImpl implements SystemDashboardService {
    @Autowired
    private SystemDashboardDao systemDashboardDao;
    
    @Override
    public List<SiteDto> getSystemDashboardSiteSegments(List<Long> idPdSites) {
        List<SiteDto> sites = new ArrayList();
        for (Long idPdSite : idPdSites) {
            SiteDto siteDto = new SiteDto();
            List<ParentSegmentDto> parentSegments = this.getSystemDashboardSiteSegments(idPdSite);
            if (parentSegments != null && !parentSegments.isEmpty()) {
                siteDto.setIdPdSite(idPdSite);
                siteDto.setParentUnitList(parentSegments);
                sites.add(siteDto);
            }
        }

        return sites;
    }
    
    @Override
    public List<DashboardDto> getDashboardLabels() {
        return this.systemDashboardDao.getLabelList();
    }
    
    private List<ParentSegmentDto> getSystemDashboardSiteSegments(Long idPdSite) {
        List<ParentSegmentDto> parentUnits = systemDashboardDao.getParentUnitList(idPdSite);
        return this.getParentSegments(parentUnits, idPdSite);
    }
    
    private List<ParentSegmentDto> getSystemDashboardSiteSegments(Long idPdSite, Long idLabel) {
        List<ParentSegmentDto> parentUnits = systemDashboardDao.getParentUnitList(idPdSite, idLabel);
        return this.getParentSegments(parentUnits, idPdSite);
    }

    private List<ParentSegmentDto> getParentSegments(List<ParentSegmentDto> parentUnits, Long idPdSite) {
        for (ParentSegmentDto parentUnit : parentUnits) {
            parentUnit.setChildUnitList(systemDashboardDao.getChildUnitList(parentUnit.getId()));
            parentUnit.setEscalated(systemDashboardDao.isUnitEscalated(idPdSite));
            for (ChildSegmentDto e : parentUnit.getChildUnitList()) {
                e.setEscalated(systemDashboardDao.isUnitEscalated(e.getId()));
            }
        }
        return parentUnits;
    }

    @Override
    public List<SiteDto> getSystemDashboardSiteSegments(List<Long> idPdSites, Long idLabel) {
        List<SiteDto> sites = new ArrayList();
        for (Long idPdSite : idPdSites) {
            SiteDto siteDto = new SiteDto();
            List<ParentSegmentDto> parentSegments = this.getSystemDashboardSiteSegments(idPdSite, idLabel);
            if (parentSegments != null && !parentSegments.isEmpty()) {
                siteDto.setIdPdSite(idPdSite);
                siteDto.setParentUnitList(parentSegments);
                sites.add(siteDto);
            }
        }

        return sites;
    }
    
    @Override
    public List<SiteDto> getSystemDashboardContractSegments(Long idLabel) {
        List<Long> idPdSites = systemDashboardDao.getPdSiteList(idLabel);
        return this.getSystemDashboardSiteSegments(idPdSites, idLabel);
    }

}
