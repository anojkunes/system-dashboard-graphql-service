package com.ts.systemdashboard.service.graphql;

import static com.ts.systemdashboard.component.dataloader.RowIndicatorDataLoaderFactory.INDICATOR_DATA_LOADER;

import com.ts.boot.uaastarter.auth.TokenInfoDetails;
import com.ts.systemdashboard.component.CustomObjectValidator;
import com.ts.systemdashboard.component.auth.TokenPrincipal;
import com.ts.systemdashboard.dto.input.CreateIndicatorDto;
import com.ts.systemdashboard.dto.view.IndicatorDto;
import com.ts.systemdashboard.dto.view.dashboard.ChildSegmentDto;
import com.ts.systemdashboard.dto.view.table.TableRowDto;
import com.ts.systemdashboard.dto.view.table.TableUiDto;
import com.ts.systemdashboard.service.TableUiService;
import com.ts.systemdashboard.validation.CreateGroup;
import io.leangen.graphql.annotations.*;
import io.leangen.graphql.execution.ResolutionEnvironment;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import lombok.extern.slf4j.Slf4j;
import org.dataloader.DataLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import io.leangen.graphql.annotations.GraphQLNonNull;

/**
 *
 * @author anoj
 */
// TODO authorization GraphQLFieldVisibility for who can see the fields and who can  fetch the fields
@Slf4j
@GraphQLApi
@Service
public class TableUiGraphQLService {
    
    @Autowired
    private TableUiService tableUiService;
    @Autowired
    private CustomObjectValidator customValidator;

    // TODO selection optimization queries and fetch
    // data loader n + 1
    // without graphql context
    // TODO paginate
    @GraphQLQuery(name = "tables")
    public List<TableUiDto> getTableUiDto(@TokenPrincipal TokenInfoDetails tokenInfoDetails, @GraphQLArgument(name="idPdUnit") Long idPdUnit, @GraphQLEnvironment ResolutionEnvironment env) {
        return tableUiService.getTableUiDto(idPdUnit);
    }

    // example of pre authorize
    @PreAuthorize("@accessAuth.hasRoleUpdateStatus(#tokenInfoDetails)")
    @GraphQLQuery(name = "tables")
    public List<TableUiDto> getChildSegmentTableUiDto(@TokenPrincipal TokenInfoDetails tokenInfoDetails, @GraphQLContext ChildSegmentDto childSegment, @GraphQLEnvironment ResolutionEnvironment env) {
        return tableUiService.getTableUiDto(childSegment.getId());
    }

    @GraphQLQuery(name = "indicators")
    public CompletableFuture<List<IndicatorDto>> getIndicators(@GraphQLContext TableRowDto tableRow, @GraphQLEnvironment ResolutionEnvironment env) {
        DataLoader indicatorDataLoader = env.dataFetchingEnvironment.getDataLoader(INDICATOR_DATA_LOADER);

        if (tableRow.getIdTableRow().equals(10687L) || tableRow.getIdTableRow().equals(19763L) ||
                tableRow.getIdTableRow().equals(721L) || tableRow.getIdTableRow().equals(11137L)) {
            throw new RuntimeException("Error is thrown here");
        }
        return indicatorDataLoader.load(tableRow.getIndicatorIds());
    }

    // Example of mutation
    @GraphQLMutation(name = "createIndicator", description = "Create Indicator entity.")
    public CreateIndicatorDto createIndicator(@GraphQLArgument(name = "inputIndicator") @GraphQLNonNull CreateIndicatorDto indicatorDto) {
        customValidator.validateObjectList(Arrays.asList(indicatorDto), CreateGroup.class);
        return indicatorDto;
    }
}
