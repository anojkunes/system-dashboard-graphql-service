package com.ts.systemdashboard.service.graphql;

import com.ts.systemdashboard.dto.view.dashboard.DashboardDto;
import com.ts.systemdashboard.dto.view.dashboard.SiteDto;
import com.ts.systemdashboard.service.SystemDashboardService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLContext;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.bval.jsr.ApacheValidationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Validation;
import javax.validation.Validator;

/**
 *
 * @author anoj
 */
@GraphQLApi
@Service
public class SystemDashboardGraphQLService {
    @Autowired
    private SystemDashboardService systemDashboardService;

    @GraphQLQuery(name = "dashboard")
    public List<DashboardDto> dashboard(@GraphQLArgument(name = "idLabels") List<Long> idLabels, @GraphQLEnvironment ResolutionEnvironment env) {
        if (idLabels != null && !idLabels.isEmpty()) {
            
            return idLabels.stream()
                    .map(idLabel -> new DashboardDto(idLabel))
                    .collect(Collectors.toList());
        }
        return systemDashboardService.getDashboardLabels();
    }

    // TODO use data loader for segments and sites
    @GraphQLQuery(name = "sites")
    public List<SiteDto> sites(@GraphQLContext DashboardDto dashboardDto, @GraphQLArgument(name="idPdSites") List<Long> idPdSites, @GraphQLEnvironment ResolutionEnvironment env) {
        if (idPdSites != null && !idPdSites.isEmpty()) {
            return systemDashboardService.getSystemDashboardSiteSegments(idPdSites, dashboardDto.getIdLabel());
        } else {
            return systemDashboardService.getSystemDashboardContractSegments(dashboardDto.getIdLabel());
        }
    }
}
