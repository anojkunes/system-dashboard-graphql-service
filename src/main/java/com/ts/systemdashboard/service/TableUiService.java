package com.ts.systemdashboard.service;

import com.ts.systemdashboard.dto.view.table.TableUiDto;
import java.util.List;

/**
 *
 * @author anoj
 */
public interface TableUiService {
    List<TableUiDto> getTableUiDto(Long idPdUnit);
}
