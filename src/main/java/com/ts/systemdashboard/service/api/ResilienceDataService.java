package com.ts.systemdashboard.service.api;

import com.ts.boot.uaastarter.auth.CustomOAuth2RestTemplate;
import com.ts.systemdashboard.dto.view.IndicatorDto;
import com.ts.systemdashboard.dto.view.PaginatedResponseDto;

import java.util.List;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author anoj
 */
@Slf4j
@Service
public class ResilienceDataService {

    @Value("${res.data.service.api.url}")
    private String resDataServiceApiUrl;

    @Autowired
    private CustomOAuth2RestTemplate oAuth2RestTemplate;

    public List<IndicatorDto> getIndicatorDetails(Set<Integer> indicatorIds) {
        final String url = resDataServiceApiUrl + "/api/v1/indicators";

        log.info("rds indicator API: {}", url);

        String indicatorIdValues = indicatorIds.toString().replace("[", "").replace("]", "")
            .replace(", ", ", ");

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("disablePagination", true);
        builder.queryParam("indicatorIds", indicatorIdValues);
        ResponseEntity<PaginatedResponseDto<IndicatorDto>> response = oAuth2RestTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, new HttpEntity<>(""),
                new ParameterizedTypeReference<PaginatedResponseDto<IndicatorDto>>() {
        });

        return response.getBody().getResults();
    }
}
