package com.ts.systemdashboard.service;

import com.ts.systemdashboard.dto.view.dashboard.DashboardDto;
import com.ts.systemdashboard.dto.view.dashboard.SiteDto;
import java.util.List;

/**
 *
 * @author anoj
 */
public interface SystemDashboardService {
    List<SiteDto> getSystemDashboardSiteSegments(List<Long> idPdSites, Long idLabel);
    List<SiteDto> getSystemDashboardSiteSegments(List<Long> idPdSites);
    List<SiteDto> getSystemDashboardContractSegments(Long idLabel);
    List<DashboardDto> getDashboardLabels();
}
