package com.ts.systemdashboard.service;

// TODO use data loader for GRAPHQL n+1

import com.ts.systemdashboard.dto.view.table.TableCellDto;
import com.ts.systemdashboard.dto.view.table.TableColumnDto;
import com.ts.systemdashboard.dto.view.table.TableRowDto;
import com.ts.systemdashboard.dto.view.table.TableUiDto;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.stream.Collectors;
import com.ts.systemdashboard.dao.TableDao;

// add graph api
@Slf4j
@Service
public class TableUiServiceImpl implements TableUiService {
    private final TableDao systemDashboardDao;

    @Autowired
    public TableUiServiceImpl(TableDao systemDashboardDao) {
        this.systemDashboardDao = systemDashboardDao;
    }

    @Override
    public List<TableUiDto> getTableUiDto(Long idPdUnit) {
        List<TableUiDto> tableUIDTOList = this.formTableData(systemDashboardDao.getTableUIForIdPdUnit(idPdUnit));
        return tableUIDTOList;
    }

    public List<TableUiDto> formTableData(List<TableUiDto> tableUIDTOList) {
        for (TableUiDto tableUIDTO : tableUIDTOList) {
            List<TableRowDto> tableRowList = systemDashboardDao.getTableRowList(tableUIDTO.getIdTable());
            List<TableColumnDto> tableColumnList = systemDashboardDao.getTableColumnList(tableUIDTO.getIdTable());
            List<TableCellDto> tableCellList = systemDashboardDao.getTableCellList(tableUIDTO.getIdTable());

            for (int i = 0; i < tableRowList.size(); i++) {
                List<Integer> idIndicatorList = new ArrayList<>();
                for (int j = 0; j < tableColumnList.size(); j++) {
                    Integer indicatorIndex = tableCellList.indexOf(new TableCellDto(tableRowList.get(i).getIdTableRow(), tableColumnList.get(j).getIdTableColumn()));
                    if (indicatorIndex > -1) {
                        idIndicatorList.add(tableCellList.get(indicatorIndex).getIdIndicator());
                    }
                }

                //remove nulls and sort
                idIndicatorList = idIndicatorList.stream().filter(p -> p != null).collect(Collectors.toList());
                idIndicatorList.sort(Integer::compareTo);

                tableRowList.get(i).setIndicatorIds(idIndicatorList);
                if (idIndicatorList.size() > 0) {
                    tableRowList.get(i).setComparisonGraphKey(idIndicatorList.stream().map(String::valueOf).collect(Collectors.joining("-")));
                }

                //set row group name
                if (tableRowList.get(i).getGroupName() == null) {
                    //get single indicator group name or set message not set
                    tableRowList.get(i).setGroupName(systemDashboardDao.getTableRowGroupName(tableRowList.get(i).getIdTableRow()));
                }
            }

            tableUIDTO.setRows(tableRowList);
            tableUIDTO.setColumns(tableColumnList);
        }
        
        return tableUIDTOList;
    }

}
