package com.ts.systemdashboard.typemapper;

import graphql.schema.GraphQLInputType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLScalarType;
import io.leangen.graphql.generator.BuildContext;
import io.leangen.graphql.generator.OperationMapper;
import io.leangen.graphql.generator.mapping.TypeMapper;
import java.lang.reflect.AnnotatedType;
import java.util.Set;
import org.joda.time.DateTime;

/**
 *
 * @author anoj
 */
public class JodaDateTimeTypeMapper implements TypeMapper {
    
    private static final GraphQLScalarType type = new JodaDateTimeScalar();

    @Override
    public GraphQLOutputType toGraphQLType(AnnotatedType javaType, OperationMapper operationMapper, Set<Class<? extends TypeMapper>> mappersToSkip, BuildContext buildContext) {
        return type;
    }

    @Override
    public GraphQLInputType toGraphQLInputType(AnnotatedType javaType, OperationMapper operationMapper, Set<Class<? extends TypeMapper>> mappersToSkip, BuildContext buildContext) {
        return type;
    }

    @Override
    public boolean supports(AnnotatedType type) {
        return type.getType() == DateTime.class;
    }
    
}
