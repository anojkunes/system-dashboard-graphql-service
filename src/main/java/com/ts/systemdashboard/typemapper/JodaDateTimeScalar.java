package com.ts.systemdashboard.typemapper;

import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;
import java.util.Optional;

import java.util.function.Function;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

// based on new graphql.scalars.datetime.DateTimeScalar()
public class JodaDateTimeScalar extends GraphQLScalarType {
    
    public JodaDateTimeScalar() {
        super("DateTime", "An RFC-3339 compliant DateTime Scalar", new Coercing<DateTime, String>() {
            DateTimeFormatter patternFormat = new DateTimeFormatterBuilder()
                            .appendPattern("yyyy-MM-dd'T'HH:mm:ssZ")
                            .toFormatter();

            @Override
            public String serialize(Object input) throws CoercingSerializeException {
                DateTime offsetDateTime;
                
                if (input instanceof DateTime) {
                    offsetDateTime = (DateTime) input;
                } else if (input instanceof String) {
                    offsetDateTime = parseOffsetDateTime(input.toString(), CoercingSerializeException::new);
                } else {
                    throw new CoercingSerializeException(
                            "Expected something we can convert to 'org.joda.time.DateTime' but was '" + (input) + "'."
                    );
                }
                try {
                    
                    String formattedDate = Optional.ofNullable(offsetDateTime)
                    .map(value -> value.toString(patternFormat)).orElse(null);
                    return formattedDate;
                } catch (Exception e) {
                    throw new CoercingSerializeException(
                            "Unable to turn TemporalAccessor into DateTime because of : '" + e.getMessage() + "'."
                    );
                }
            }

            @Override
            public DateTime parseValue(Object input) throws CoercingParseValueException {
                DateTime offsetDateTime;
                if (input instanceof DateTime) {
                    offsetDateTime = (DateTime) input;
                } else if (input instanceof String) {
                    offsetDateTime = parseOffsetDateTime(input.toString(), CoercingParseValueException::new);
                } else {
                    throw new CoercingParseValueException(
                            "Expected a 'String' but was '" + (input) + "'."
                    );
                }
                return offsetDateTime;
            }

            @Override
            public DateTime parseLiteral(Object input) throws CoercingParseLiteralException {
                if (!(input instanceof StringValue)) {
                    throw new CoercingParseLiteralException(
                            "Expected AST type 'StringValue' but was '" + (input) + "'."
                    );
                }
                return parseOffsetDateTime(((StringValue) input).getValue(), CoercingParseLiteralException::new);
            }

            private DateTime parseOffsetDateTime(String s, Function<String, RuntimeException> exceptionMaker) {
                try {
                    return DateTime.parse(s, patternFormat);
                } catch (Exception e) {
                    throw exceptionMaker.apply("Invalid RFC3339 value : '" + s + "'. because of : '" + e.getMessage() + "'");
                }
            }
        });
    }

}
