package com.ts.systemdashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// TODO research graphql stitching for graphql microservices - java nadel
@SpringBootApplication
public class Application {
    // integration tests on graphql based on:
    // https://github.com/leangen/graphql-spqr-spring-boot-starter/blob/master/graphql-spqr-spring-boot-autoconfigure/src/test/java/io/leangen/graphql/spqr/spring/web/GraphQLControllerTest.java
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
