package com.ts.systemdashboard._config;

import com.ts.systemdashboard.component.argument.TokenPrincipalArgumentInjector;
import com.ts.systemdashboard.typemapper.JodaDateTimeTypeMapper;
import com.ts.systemdashboard.exception.CustomExceptionHandler;
import graphql.GraphQL;
import graphql.execution.AsyncExecutionStrategy;
import graphql.execution.AsyncSerialExecutionStrategy;
import graphql.schema.GraphQLSchema;
import io.leangen.graphql.ExtensionProvider;
import io.leangen.graphql.GeneratorConfiguration;
import io.leangen.graphql.generator.mapping.ArgumentInjector;
import io.leangen.graphql.generator.mapping.TypeMapper;
import io.leangen.graphql.generator.mapping.common.IdAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author anoj
 */
@Configuration
public class GraphQLConfig {
    // based on https://github.com/leangen/graphql-spqr-spring-boot-starter
    @Bean
    public ExtensionProvider<GeneratorConfiguration, TypeMapper> customTypeMappers() {
        //Insert a custom converter after the built-in IdAdapter (which is generally a safe position).
         //Return a new list instead to take full control. 
        return (config, current) -> current.insertAfter(IdAdapter.class, new JodaDateTimeTypeMapper());
    }

    @Bean
    public GraphQL graphQL(GraphQLSchema schema) {
        return GraphQL.newGraphQL(schema)
                .queryExecutionStrategy(new AsyncExecutionStrategy(new CustomExceptionHandler()))
                .mutationExecutionStrategy(new AsyncSerialExecutionStrategy(new CustomExceptionHandler()))
                .build();
    }

    @Bean
    public ExtensionProvider<GeneratorConfiguration, ArgumentInjector> argumentInjectorExtensionProvider() {
        return (config, current) -> current.prepend(
                new TokenPrincipalArgumentInjector()
        );
    }
}
