package com.ts.systemdashboard.dto.view.table;

import lombok.Data;

@Data
public class TableCellDto {
    private Long idRow;
    private Long idColumn;
    private Integer idIndicator;

    public TableCellDto() {
    }

    public TableCellDto(Long idRow, Long idColumn) {
        this.idColumn = idColumn;
        this.idRow = idRow;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TableCellDto that = (TableCellDto) o;

        if (!idRow.equals(that.idRow)) {
            return false;
        }
        return idColumn.equals(that.idColumn);

    }

    @Override
    public int hashCode() {
        int result = idRow.hashCode();
        result = 31 * result + idColumn.hashCode();
        return result;
    }
}
