package com.ts.systemdashboard.dto.view.dashboard;

import lombok.Data;

/**
 *
 * @author anoj
 */
@Data
public class DashboardDto {
    private Long idLabel;
    
    public DashboardDto() {}
    
    public DashboardDto(Long idLabel) {
        this.idLabel = idLabel;
    }
}
