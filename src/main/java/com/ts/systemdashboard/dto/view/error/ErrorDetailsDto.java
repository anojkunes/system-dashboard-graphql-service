package com.ts.systemdashboard.dto.view.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorDetailsDto {
    private Object value;
    private String error;

    public ErrorDetailsDto() {
    }

    public ErrorDetailsDto(Object value, String error) {
        this.value = value;
        this.error = error;
    }
}
