package com.ts.systemdashboard.dto.view.table;

import lombok.Data;

import java.util.List;

@Data
public class TableRowDto {
    private Long idTableRow;
    private String groupName;
    private List<Integer> indicatorIds;
//    private List<IndicatorDto> indicators; // TODO use rest api call
    private String comparisonGraphKey;
}
