/**
 * 
 */
package com.ts.systemdashboard.dto.view;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

/**
 * @author Vicky
 *
 */
@Data
@JsonInclude(value = Include.NON_NULL)
public class PaginatedResponseDto<T> {
    private Integer page;
    private Long totalRecords;
    private Integer limit;
    private List<T> results;
}
