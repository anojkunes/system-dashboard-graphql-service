package com.ts.systemdashboard.dto.view.table;

import lombok.Data;

@Data
public class TableColumnDto {
    private Long idTableColumn;
    private String title;
    private Long idLabel;
    private Long idPdUnit;
    private String colorCodeFg;
    private String colorCodeBg;
}
