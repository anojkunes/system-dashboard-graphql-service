package com.ts.systemdashboard.dto.view.table;

import lombok.Data;

import java.util.List;

@Data
public class TableUiDto {
    private Long idTable;
    private String title;
    private Integer orderId;
//    @JsonSerialize(using = JsonDateTimeSerializer.class)
//    private DateTime dateData;
    private List<TableColumnDto> columns;
    private List<TableRowDto> rows;
}
