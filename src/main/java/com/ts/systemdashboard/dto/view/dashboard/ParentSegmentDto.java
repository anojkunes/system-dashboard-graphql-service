package com.ts.systemdashboard.dto.view.dashboard;

import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author anoj
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ParentSegmentDto extends SegmentDto {
    private List<ChildSegmentDto> childUnitList;
    
//    public ParentSegmentDto(SegmentDto segmentDto) {
//        this.setId(segmentDto.getId());
//        this.setAlertLevel(segmentDto.getAlertLevel());
//        this.setColorCodeBg(segmentDto.getColorCodeBg());
//        this.setColorCodeFg(segmentDto.getColorCodeFg());
//        this.setCurrentLevel(segmentDto.getCurrentLevel());
//        this.setDescription(segmentDto.getDescription());
//        this.setEscalated(segmentDto.isEscalated());
//        this.setHasAlert(segmentDto.isHasAlert());
//        this.setIdLabel(segmentDto.getIdLabel());
//        this.setIdPdSite(segmentDto.getIdPdSite());
//        this.setName(segmentDto.getName());
//        this.setOrderId(segmentDto.getOrderId());
//        this.setShared(segmentDto.isShared());
//        this.setSiteName(segmentDto.getSiteName());
//        this.setWeightCalculation(segmentDto.getWeightCalculation());
//        this.setWeightage(segmentDto.getWeightage());
//    }
}
