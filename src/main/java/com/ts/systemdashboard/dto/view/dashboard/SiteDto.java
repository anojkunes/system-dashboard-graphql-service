package com.ts.systemdashboard.dto.view.dashboard;

import java.util.List;
import lombok.Data;

/**
 *
 * @author anoj
 */
@Data
public class SiteDto {
    Long idPdSite;
    List<ParentSegmentDto> parentUnitList;
}
