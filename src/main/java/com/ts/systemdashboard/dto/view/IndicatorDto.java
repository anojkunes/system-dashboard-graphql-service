package com.ts.systemdashboard.dto.view;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ts.systemdashboard.util.JsonDateTimeSerializer;
import lombok.Data;
import org.joda.time.DateTime;


@Data
public class IndicatorDto {
    private Integer indicatorId;
    private String indicatorName;
    private Integer labelId;
    private String labelName;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    private DateTime dateData = new DateTime();

    public IndicatorDto() {}
}
