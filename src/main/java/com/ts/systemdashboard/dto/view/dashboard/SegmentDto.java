package com.ts.systemdashboard.dto.view.dashboard;

import lombok.Data;

/**
 *
 * @author anoj
 */
@Data
public class SegmentDto {
    protected Long id;
    protected String name;
    protected String description;
    protected String siteName;
    protected Long idPdSite;
    protected Long idLabel;
    protected boolean shared;
    protected boolean hasAlert;
    protected Integer weightCalculation;
    protected Integer weightage;
    protected Integer alertLevel;
    protected int orderId;
    protected Integer currentLevel;
    protected String colorCodeFg;
    protected String colorCodeBg;
    protected boolean isEscalated;
}
