package com.ts.systemdashboard.dto.input;

import com.ts.systemdashboard.validation.CreateGroup;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class UserDto {
    @NotEmpty(groups = {CreateGroup.class })
    @Min(3)
    private String fullName;
    @Email(groups = { CreateGroup.class })
    private String email;
}
