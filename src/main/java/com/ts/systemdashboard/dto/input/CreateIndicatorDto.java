package com.ts.systemdashboard.dto.input;

import com.ts.systemdashboard.dto.view.IndicatorDto;
import com.ts.systemdashboard.validation.CreateGroup;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;


@Data
public class CreateIndicatorDto extends IndicatorDto {
    @NotNull(groups = { CreateGroup.class })
    public Integer getIndicatorId() {
        return super.getIndicatorId();
    }

    @NotEmpty(groups = { CreateGroup.class })
    public String getIndicatorName() { return super.getIndicatorName(); }

    @Valid
    private List<UserDto> users;
}
