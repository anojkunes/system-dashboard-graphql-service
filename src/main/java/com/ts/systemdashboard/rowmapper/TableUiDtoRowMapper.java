package com.ts.systemdashboard.rowmapper;

import com.ts.systemdashboard.dto.view.table.TableUiDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TableUiDtoRowMapper implements RowMapper<TableUiDto> {
    @Override
    public TableUiDto mapRow(ResultSet rs, int i) throws SQLException {
        TableUiDto tableUiDto = new TableUiDto();
        tableUiDto.setIdTable(rs.getLong("idTable"));
        tableUiDto.setTitle(rs.getString("title"));
        tableUiDto.setOrderId(rs.getInt("orderId"));
        return tableUiDto;
    }
}
