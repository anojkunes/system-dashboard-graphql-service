package com.ts.systemdashboard.rowmapper;

import com.ts.systemdashboard.dto.view.table.TableColumnDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TableColumnDtoRowMapper implements RowMapper<TableColumnDto> {
    @Override
    public TableColumnDto mapRow(ResultSet rs, int i) throws SQLException {
        TableColumnDto tableColumnDto = new TableColumnDto();
        tableColumnDto.setIdTableColumn(rs.getLong("pduc.id_pd_unit_table_column"));
        tableColumnDto.setTitle(rs.getString("pduc.title"));

        //check if label is assigned. then take labels current color
        if(rs.getObject("pduc.id_label_title") !=null && rs.getObject("ccl.id_color_code") !=null) {
            tableColumnDto.setIdPdUnit(rs.getLong("pdu.id_label_title"));
            tableColumnDto.setColorCodeFg(rs.getString("ccl.color_code_hex_fg"));
            tableColumnDto.setColorCodeBg(rs.getString("ccl.color_code_hex_bg"));
        }

        //check if pd_unit is assigned. then take pd_unit current color
        if(rs.getObject("pduc.id_pd_unit_title") !=null && rs.getObject("ccp.id_color_code") !=null) {
            tableColumnDto.setIdPdUnit(rs.getLong("pdu.id_pd_unit_title"));
            tableColumnDto.setColorCodeFg(rs.getString("ccp.color_code_hex_fg"));
            tableColumnDto.setColorCodeBg(rs.getString("ccp.color_code_hex_bg"));
        }

        return tableColumnDto;
    }
}
