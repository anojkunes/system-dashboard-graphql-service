package com.ts.systemdashboard.rowmapper;

import com.ts.systemdashboard.dto.view.table.TableCellDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TableCellDtoRowMapper implements RowMapper<TableCellDto> {

    @Override
    public TableCellDto mapRow(ResultSet rs, int i) throws SQLException {
        TableCellDto tableCellDto = new TableCellDto();
        tableCellDto.setIdRow(rs.getLong("r.id_pd_unit_table_row"));
        tableCellDto.setIdColumn(rs.getLong("c.id_pd_unit_table_column"));
        
        if (rs.getObject("i.id_indicator") != null) {
            tableCellDto.setIdIndicator(rs.getInt("i.id_indicator"));
        }

        return tableCellDto;
    }
}
