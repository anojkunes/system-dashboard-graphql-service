package com.ts.systemdashboard.rowmapper;

import com.ts.systemdashboard.dto.view.table.TableRowDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TableRowDtoRowMapper implements RowMapper<TableRowDto> {
    @Override
    public TableRowDto mapRow(ResultSet rs, int i) throws SQLException {
        TableRowDto tableRowDto = new TableRowDto();
        tableRowDto.setIdTableRow(rs.getLong("idTableRow"));
        tableRowDto.setGroupName(rs.getString("groupName"));
        return tableRowDto;
    }
}
