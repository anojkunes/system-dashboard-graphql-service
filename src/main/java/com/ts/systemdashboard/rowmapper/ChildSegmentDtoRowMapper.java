package com.ts.systemdashboard.rowmapper;

import com.ts.systemdashboard.dto.view.dashboard.ChildSegmentDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author anoj
 */
public class ChildSegmentDtoRowMapper implements RowMapper<ChildSegmentDto> {

    @Override
    public ChildSegmentDto mapRow(ResultSet rs, int i) throws SQLException {
        ChildSegmentDto item = new ChildSegmentDto();
        item.setId(rs.getLong("pdu.id_pd_unit"));
        item.setName(rs.getString("pdu.name"));
        item.setDescription(rs.getString("pdu.description"));
        item.setIdPdSite(rs.getLong("pds.id_pd_site"));
        item.setIdLabel(rs.getLong("pds.id_label"));
        item.setSiteName(rs.getString("pds.menu_name"));
        item.setShared(rs.getBoolean("pds.shared"));
        item.setHasAlert(rs.getBoolean("pdu.has_alert"));
        item.setWeightCalculation(rs.getInt("pdu.weight_calculation"));
        item.setAlertLevel(rs.getInt("pdu.alert_level"));
        item.setOrderId(rs.getInt("pdu.order_id"));

        if (rs.getObject("pduhu.weightage") != null) {
            item.setWeightage(rs.getInt("pduhu.weightage"));
        }

        //TODO, need to be inner join
        if(rs.getObject("pdl.id_pd_level")!=null) {
            item.setCurrentLevel(rs.getInt("pdl.level"));
            item.setColorCodeFg(rs.getString("cc.color_code_hex_fg"));
            item.setColorCodeBg(rs.getString("cc.color_code_hex_bg"));
        }

        return item;
    }
    
}
