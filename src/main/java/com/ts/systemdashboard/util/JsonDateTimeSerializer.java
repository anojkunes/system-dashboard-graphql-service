package com.ts.systemdashboard.util;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import java.io.IOException;
import java.util.Optional;

public class JsonDateTimeSerializer extends JsonSerializer<DateTime> {

    @Override
    public void serialize(DateTime dateTime, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
        DateTimeFormatter patternFormat = new DateTimeFormatterBuilder()
                .appendPattern("yyyy-MM-dd'T'HH:mm:ssZ")
                .toFormatter();

        try {
            String formattedDate = Optional.ofNullable(dateTime)
                    .map(value -> value.toString(patternFormat)).orElse(null);

            jsonGenerator.writeString(formattedDate);
        } catch (Exception ex) {
            String message = String.format("%s field with value %s could not be parsed. Check whether %s field is in yyyy-MM-dd'T'HH:mm:ssZ' format",
                    jsonGenerator.getOutputContext().getCurrentName(),
                    dateTime,
                    jsonGenerator.getOutputContext().getCurrentName());

            throw new IllegalArgumentException(message);
        }
    }
}

