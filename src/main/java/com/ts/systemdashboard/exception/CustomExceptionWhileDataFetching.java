package com.ts.systemdashboard.exception;

import static graphql.Assert.assertNotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ts.systemdashboard.dto.view.error.ErrorDetailsDto;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.GraphqlErrorHelper;
import graphql.execution.ExecutionPath;
import graphql.language.SourceLocation;
import static java.lang.String.format;

import java.util.*;

/**
 *
 * @author anoj
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomExceptionWhileDataFetching implements GraphQLError {
    private String type;
    private final List<Object> path;
    private final Throwable exception;
    private final List<SourceLocation> locations;
    private Map<String, Object> extensions; // add all error

    public CustomExceptionWhileDataFetching(ExecutionPath path, Throwable exception, SourceLocation sourceLocation) {
        this.path = assertNotNull(path).toList();
        this.exception = assertNotNull(exception);
        this.locations = Collections.singletonList(sourceLocation);
        mkExtensions(exception);
    }

    private String mkMessage(ExecutionPath path, Throwable exception) {
        return format(exception.getMessage());
    }

    /*
     * This allows a DataFetcher to throw a graphql error and have "extension data" be transferred from that
     * exception into the ExceptionWhileDataFetching error and hence have custom "extension attributes"
     * per error message.
     */
    private void mkExtensions(Throwable exception) {
        Map<String, Object> extensions = null;
        if (exception instanceof GraphQLError) {
            this.type = "GRAPHQL ERROR";
            Map<String, Object> map = ((GraphQLError) exception).getExtensions();
            if (map != null) {
                extensions = new LinkedHashMap<>();
                extensions.putAll(Collections.singletonMap("details", Arrays.asList(new ErrorDetailsDto(null, exception.getMessage()))));
            }
        } else if(exception instanceof DataValidationException) {
            this.type = "VALIDATION ERROR";
            DataValidationException ex = (DataValidationException) exception;
            extensions = new LinkedHashMap<>();
            extensions.putAll(Collections.singletonMap("details", ex.getErrors()));

        } else {
            this.type = "INTERNAL SERVER ERROR";
            extensions = new LinkedHashMap<>();
            extensions.putAll(Collections.singletonMap("details", Arrays.asList(new ErrorDetailsDto(null, exception.getMessage()))));
        }

        this.extensions = extensions;
    }

    public Throwable getException() {
        return exception;
    }

    @Override
    public String getMessage() {
        return type;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return locations;
    }

    @Override
    public List<Object> getPath() {
        return path;
    }

    @Override
    public Map<String, Object> getExtensions() {
        return extensions;
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.DataFetchingException;
    }

    @Override
    public String toString() {
        return "ExceptionWhileDataFetching{" +
                "path=" + path +
                "exception=" + exception +
                "locations=" + locations +
                '}';
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object o) {
        return GraphqlErrorHelper.equals(this, o);
    }

    @Override
    public int hashCode() {
        return GraphqlErrorHelper.hashCode(this);
    }
}
