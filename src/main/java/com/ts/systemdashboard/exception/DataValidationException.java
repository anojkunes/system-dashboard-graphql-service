package com.ts.systemdashboard.exception;

import com.ts.systemdashboard.dto.view.error.ErrorDetailsDto;

import java.util.List;

public class DataValidationException extends RuntimeException {
    private List<ErrorDetailsDto> errors;

    public DataValidationException(List<ErrorDetailsDto> errors) {
        super("Validation Exception occurred");
        this.errors = errors;
    }

    public List<ErrorDetailsDto> getErrors() {
        return this.errors;
    }
}
