package com.ts.systemdashboard.dao;

import com.ts.systemdashboard.dto.view.table.TableCellDto;
import com.ts.systemdashboard.dto.view.table.TableColumnDto;
import com.ts.systemdashboard.dto.view.table.TableRowDto;
import com.ts.systemdashboard.dto.view.table.TableUiDto;
import com.ts.systemdashboard.rowmapper.TableCellDtoRowMapper;
import com.ts.systemdashboard.rowmapper.TableColumnDtoRowMapper;
import com.ts.systemdashboard.rowmapper.TableRowDtoRowMapper;
import com.ts.systemdashboard.rowmapper.TableUiDtoRowMapper;
import org.springframework.stereotype.Repository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.dao.DataAccessException;

@Repository
public class TableDaoImpl extends ShrewdDao implements TableDao {
    @Override
    public List<TableUiDto> getTableUIForIdPdUnit(Long idPdUnit) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("idPdUnit", idPdUnit);

        String sql = "SELECT id_pd_unit_table AS idTable, title, order_id AS orderId " +
                "FROM pd_unit_table " +
                "WHERE id_pd_unit = :idPdUnit " +
                "AND active = TRUE " +
                "AND live = TRUE " +
                "ORDER BY order_id ASC, date_updated ASC";

        return namedJdbcTemplate.query(sql, namedParameters, new TableUiDtoRowMapper());
    }

    @Override
    public List<TableRowDto> getTableRowList(Long idTable) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("idTable", idTable);

        String sql = "SELECT pdutr.id_pd_unit_table_row AS idTableRow, ig.name AS groupName " +
                "FROM pd_unit_table_row pdutr " +
                "LEFT JOIN indicator_group ig ON ig.id_indicator_group = pdutr.id_indicator_group " +
                "WHERE pdutr.id_pd_unit_table = :idTable " +
                "AND pdutr.active = TRUE " +
                "ORDER BY pdutr.order_id ASC";

        return namedJdbcTemplate.query(sql, namedParameters, new TableRowDtoRowMapper());
    }

    @Override
    public List<TableColumnDto> getTableColumnList(Long idTable) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("idTable", idTable);

        String sql = "SELECT * " +
                "FROM pd_unit_table_column pduc " +
                "LEFT JOIN label l ON l.id_label = pduc.id_label_title " +
                "LEFT JOIN current_label_color_status clcs ON clcs.id_label = l.id_label " +
                "LEFT JOIN color_code ccl ON ccl.id_color_code = clcs.id_color_code " +
                "LEFT JOIN pd_unit pdu ON pdu.id_pd_unit = pduc.id_pd_unit_title " +
                "LEFT JOIN pd_level pdl ON pdl.id_pd_level = pdu.id_pd_level_current " +
                "LEFT JOIN color_code ccp ON ccp.id_color_code = pdl.id_color_code " +
                "WHERE pduc.id_pd_unit_table = :idTable " +
                "AND pduc.active = TRUE " +
                "ORDER BY pduc.order_id ASC";

        return namedJdbcTemplate.query(sql, namedParameters, new TableColumnDtoRowMapper());
    }

    @Override
    public List<TableCellDto> getTableCellList(Long idTable) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("idTable", idTable);

        String sql = "SELECT * " +
                "FROM pd_unit_table pdut " +
                "INNER JOIN pd_unit_table_row r ON r.id_pd_unit_table = pdut.id_pd_unit_table " +
                "INNER JOIN pd_unit_table_column c ON c.id_pd_unit_table = pdut.id_pd_unit_table " +
                "LEFT JOIN pd_unit_has_indicator pduhi ON pduhi.id_pd_unit_table_column = c.id_pd_unit_table_column AND pduhi.id_pd_unit_table_row = r.id_pd_unit_table_row " +
                "LEFT JOIN indicator i ON i.id_indicator = pduhi.id_indicator " +
                "LEFT JOIN current_indicator_status cis ON cis.id_indicator = i.id_indicator " +
                "LEFT JOIN threshold t ON t.id_threshold = cis.id_threshold " +
                "LEFT JOIN color_code cc ON cc.id_color_code = t.id_color_code " +
                "WHERE pdut.id_pd_unit_table = :idTable " +
                "AND pduhi.active = TRUE " +
                "AND i.active = TRUE " +
                "ORDER BY r.order_id ASC, c.order_id ASC";

        return namedJdbcTemplate.query(sql, namedParameters, new TableCellDtoRowMapper());
    }

    @Override
    public String getTableRowGroupName(Long idTableRow) {
        String sql = "SELECT i.name FROM pd_unit_has_indicator pduhi "
                + "INNER JOIN indicator i ON i.id_indicator = pduhi.id_indicator "
                + "WHERE pduhi.id_pd_unit_table_row = :idTableRow "
                + "AND pduhi.active = TRUE "
                + "LIMIT 1";

        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("idTableRow", idTableRow);

        try {
            return namedJdbcTemplate.queryForObject(sql, namedParameters, String.class);
        } catch (DataAccessException expected) {
        }

        return null;
    }
}
