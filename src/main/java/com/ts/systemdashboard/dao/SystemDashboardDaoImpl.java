package com.ts.systemdashboard.dao;

import com.ts.systemdashboard.dto.view.dashboard.ParentSegmentDto;
import com.ts.systemdashboard.dto.view.dashboard.ChildSegmentDto;
import com.ts.systemdashboard.dto.view.dashboard.DashboardDto;
import com.ts.systemdashboard.rowmapper.ChildSegmentDtoRowMapper;
import com.ts.systemdashboard.rowmapper.ParentSegmentDtoRowMapper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

/**
 *
 * @author anoj
 */
@Repository
public class SystemDashboardDaoImpl extends ShrewdDao implements SystemDashboardDao {

    @Override
    public List<Long> getPdSiteList(Long idLabel) {
                Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("idLabel", idLabel);
        String sql = "SELECT pds.id_pd_site FROM label l "
                + "INNER JOIN pd_site pds ON pds.id_label = l.id_label "
                + "WHERE l.id_label = :idLabel "
                + "AND pds.shared = FALSE "
                + "AND pds.active = TRUE "
                + "ORDER BY pds.order_id ASC";

        return namedJdbcTemplate.queryForList(sql, namedParameters, Long.class);
    }
    
    @Override
    public List<DashboardDto> getLabelList() {
        String sql = "SELECT l.id_label FROM label l "
                + "INNER JOIN pd_site pds ON pds.id_label = l.id_label "
                + "WHERE pds.shared = FALSE "
                + "AND pds.active = TRUE";

        return namedJdbcTemplate.query(sql, (rs, i) -> {
            return new DashboardDto(rs.getLong("l.id_label"));
        });
    }

    @Override
    public List<ParentSegmentDto> getParentUnitList(Long idPdSite) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("idPdSite", idPdSite);
        
        String sql = "SELECT *, NULL AS 'pduhu.weightage' " +
                "FROM pd_unit pdu " +
                "INNER JOIN pd_site pds ON pds.id_pd_site = pdu.id_pd_site " +
                "LEFT JOIN pd_level pdl ON pdl.id_pd_level = pdu.id_pd_level_current " +
                "LEFT JOIN color_code cc ON cc.id_color_code = pdl.id_color_code " +
                "WHERE pdu.id_pd_site = :idPdSite " +
                "AND pdu.active = TRUE " +
                "AND pdu.id_pd_unit NOT IN " +
                " ( " +
                "   SELECT DISTINCT(pduhu2.id_pd_unit_child) " +
                "   FROM pd_unit_has_unit pduhu2 " +
                "   INNER JOIN pd_unit pdu2 ON pdu2.id_pd_unit = pduhu2.id_pd_unit_parent" +
                "   WHERE pduhu2.active = TRUE AND pdu2.active = TRUE AND pdu2.id_pd_site = :idPdSite " +
                " ) " +
                "ORDER BY pdu.order_id ASC";

        return namedJdbcTemplate.query(sql, namedParameters, new ParentSegmentDtoRowMapper());
    }
    
    public List<ParentSegmentDto> getParentUnitList(Long idPdSite, Long idLabel) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("idPdSite", idPdSite);
        namedParameters.put("idLabel", idLabel);
        
        String sql = "SELECT *, NULL AS 'pduhu.weightage' " +
                "FROM pd_unit pdu " +
                "INNER JOIN pd_site pds ON pds.id_pd_site = pdu.id_pd_site " +
                "LEFT JOIN pd_level pdl ON pdl.id_pd_level = pdu.id_pd_level_current " +
                "LEFT JOIN color_code cc ON cc.id_color_code = pdl.id_color_code " +
                "WHERE pdu.id_pd_site = :idPdSite " +
                "AND pds.id_label = :idLabel " +
                "AND pdu.active = TRUE " +
                "AND pdu.id_pd_unit NOT IN " +
                " ( " +
                "   SELECT DISTINCT(pduhu2.id_pd_unit_child) " +
                "   FROM pd_unit_has_unit pduhu2 " +
                "   INNER JOIN pd_unit pdu2 ON pdu2.id_pd_unit = pduhu2.id_pd_unit_parent" +
                "   WHERE pduhu2.active = TRUE AND pdu2.active = TRUE AND pdu2.id_pd_site = :idPdSite " +
                " ) " +
                "ORDER BY pdu.order_id ASC";

        return namedJdbcTemplate.query(sql, namedParameters, new ParentSegmentDtoRowMapper());
    }
    
    @Override
    public List<ChildSegmentDto> getChildUnitList(Long idPdUnitParent) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("idPdUnitParent", idPdUnitParent);

        String sql = "SELECT * "
                + "FROM pd_unit_has_unit pduhu "
                + "INNER JOIN pd_unit pdu ON pdu.id_pd_unit = pduhu.id_pd_unit_child "
                + "INNER JOIN pd_site pds ON pds.id_pd_site = pdu.id_pd_site "
                + "LEFT JOIN pd_level pdl ON pdl.id_pd_level = pdu.id_pd_level_current "
                + "LEFT JOIN color_code cc ON cc.id_color_code = pdl.id_color_code "
                + "WHERE pduhu.id_pd_unit_parent = :idPdUnitParent "
                + "AND pdu.active = TRUE AND pduhu.active = TRUE "
                + "ORDER BY pduhu.order_id ASC";

        return namedJdbcTemplate.query(sql, namedParameters, new ChildSegmentDtoRowMapper());
    }

    @Override
    public boolean isUnitEscalated(Long idUnit) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("idUnit", idUnit);

        boolean isEscalated = false;

        String sql = "SELECT count(*) FROM  pd_unit_table t INNER JOIN pd_unit_table_column c ON t.id_pd_unit_table=c.id_pd_unit_table  "
                + "INNER JOIN  pd_unit_table_row r ON  t.id_pd_unit_table=r.id_pd_unit_table "
                + "INNER JOIN pd_unit_has_indicator pduhi ON pduhi.id_pd_unit_table_column = c.id_pd_unit_table_column "
                + "and pduhi.id_pd_unit_table_row=r.id_pd_unit_table_row "
                + "INNER JOIN active_escalation_plan aep ON pduhi.id_indicator = aep.id_indicator  "
                + "WHERE t.active=TRUE AND c.active=TRUE AND r.active=TRUE AND pduhi.active=TRUE AND aep.active=TRUE "
                + "AND t.id_pd_unit=:idUnit AND pduhi.id_pd_unit=:idUnit ";

        int count = namedJdbcTemplate.queryForObject(sql, namedParameters, Integer.class);

        if (count > 0) {
            isEscalated = true;
        }
        return isEscalated;
    }
}
