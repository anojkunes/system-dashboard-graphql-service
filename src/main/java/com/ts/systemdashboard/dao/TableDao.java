package com.ts.systemdashboard.dao;

import com.ts.systemdashboard.dto.view.table.TableCellDto;
import com.ts.systemdashboard.dto.view.table.TableColumnDto;
import com.ts.systemdashboard.dto.view.table.TableRowDto;
import com.ts.systemdashboard.dto.view.table.TableUiDto;

import java.util.List;

public interface TableDao {
    List<TableUiDto> getTableUIForIdPdUnit(Long idPdUnit);
    List<TableRowDto> getTableRowList(Long idTable);
    List<TableColumnDto> getTableColumnList(Long idTable);
    List<TableCellDto> getTableCellList(Long idTable);
    String getTableRowGroupName(Long idTableRow);
}
