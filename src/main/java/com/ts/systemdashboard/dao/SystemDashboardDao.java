package com.ts.systemdashboard.dao;

import com.ts.systemdashboard.dto.view.dashboard.ChildSegmentDto;
import com.ts.systemdashboard.dto.view.dashboard.DashboardDto;
import com.ts.systemdashboard.dto.view.dashboard.ParentSegmentDto;
import java.util.List;

/**
 *
 * @author anoj
 */
public interface SystemDashboardDao {
    List<Long> getPdSiteList(Long idLabel);
    List<ParentSegmentDto> getParentUnitList(Long idPdSite);
    List<ParentSegmentDto> getParentUnitList(Long idPdSite, Long idLabel);
    List<ChildSegmentDto> getChildUnitList(Long idPdUnitParent);
    boolean isUnitEscalated(Long idUnit);
    List<DashboardDto> getLabelList();
}
