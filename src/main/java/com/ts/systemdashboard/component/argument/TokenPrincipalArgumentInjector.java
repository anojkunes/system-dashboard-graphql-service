package com.ts.systemdashboard.component.argument;

import com.ts.boot.uaastarter.auth.TokenInfoDetails;
import com.ts.systemdashboard.component.auth.TokenPrincipal;
import io.leangen.graphql.generator.mapping.ArgumentInjector;
import io.leangen.graphql.generator.mapping.ArgumentInjectorParams;
import io.leangen.graphql.spqr.spring.autoconfigure.DefaultGlobalContext;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Parameter;

@Component
public class TokenPrincipalArgumentInjector implements ArgumentInjector {
    @Override
    public TokenInfoDetails getArgumentValue(ArgumentInjectorParams params) {
        DefaultGlobalContext globalContext = params
                .getResolutionEnvironment()
                .dataFetchingEnvironment
                .getContext();

        ServletWebRequest webRequest = (ServletWebRequest) globalContext.getNativeRequest();
        OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) webRequest.getUserPrincipal();
        TokenInfoDetails tokenInfoDetails = (TokenInfoDetails) oAuth2Authentication.getPrincipal();
//        TokenInfoDetails tokenInfoDetails = (TokenInfoDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return tokenInfoDetails;
    }

    @Override
    public boolean supports(AnnotatedType type, Parameter parameter) {
        return parameter != null && parameter.isAnnotationPresent(TokenPrincipal.class);
    }
}
