package com.ts.systemdashboard.component.auth;

import com.ts.boot.uaastarter.auth.TokenInfoDetails;
import org.springframework.stereotype.Component;

@Component("accessAuth")
public class AccessAuth {
    public boolean hasRoleUpdateStatus(TokenInfoDetails tokenInfoDetails) {
        if (tokenInfoDetails.getAccessTokenType().equals(TokenInfoDetails.AccessType.USER)) {
            boolean hasRole = tokenInfoDetails.getUserInfo().getAuthorities().stream().anyMatch(x -> x.getAuthority().contains("ROLE_UPDATE_STATUS"));
            return hasRole;
        }

        return false;
    }
}
