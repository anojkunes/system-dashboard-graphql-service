package com.ts.systemdashboard.component.dataloader;

import com.ts.systemdashboard.dto.view.IndicatorDto;
import com.ts.systemdashboard.service.api.ResilienceDataService;
import io.leangen.graphql.spqr.spring.autoconfigure.DataLoaderRegistryFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.dataloader.BatchLoader;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author anoj
 */
// implement data loader registry factory
// based on https://medium.com/@bradbaker/big-movements-in-the-graphql-java-world-67629fd45508
@Slf4j
@Component
public class RowIndicatorDataLoaderFactory implements DataLoaderRegistryFactory  {
    
    public static final String INDICATOR_DATA_LOADER = "tableui_row_indicator";
    
    @Autowired
    public ResilienceDataService resilienceDataService;
    
    private BatchLoader<List<Integer>, List<IndicatorDto>> indicatorBatchLoader = indicatorIdList -> getRowIndicator(indicatorIdList);

    @Override
    public DataLoaderRegistry createDataLoaderRegistry() {
        /**
         * The default cache implementation is using an in-memory HashMap without any expiry mechanism. 
         * Once fetched the data stays there until the server is restarted or runs out of memory
         * 
         * TODO cache implementation which supports eviction based on time and size
         * using DataLoaderOptions and create custom cache map 
         * based on https://blog.softwaremill.com/graphql-dataloader-in-spring-boot-singleton-or-request-scoped-16699436f680
         */
        DataLoader<List<Integer>, List<IndicatorDto>> indicatorLoader = new DataLoader<>(indicatorBatchLoader);
//        DataLoaderOptions options = DataLoaderOptions.newOptions().setMaxBatchSize(100);
//        DataLoader<List<Integer>, List<IndicatorDto>> indicatorLoader = new DataLoader<>(indicatorBatchLoader, options);
        DataLoaderRegistry loaders = new DataLoaderRegistry();
        loaders.register(INDICATOR_DATA_LOADER, indicatorLoader);
        return loaders;
    }
    
    private CompletableFuture<List<List<IndicatorDto>>> getRowIndicator(List<List<Integer>> listOfIndicatorIdList) {
        log.info("listOfIndicatorIdList size: {}", listOfIndicatorIdList.size());
        return CompletableFuture.supplyAsync(() -> this.getListOfIndicatorDtoList(listOfIndicatorIdList));
    }
    
    private List<List<IndicatorDto>> getListOfIndicatorDtoList(List<List<Integer>> listOfIndicatorIdList) {
        Set<Integer> indicatorIdList = listOfIndicatorIdList.stream()
                .flatMap(x -> x.stream())
                .collect(Collectors.toSet());
        
        log.info("indicatorIdList: {}", indicatorIdList);

        // batch call Web Call
        List<IndicatorDto> indicatorDtoList = this.resilienceDataService.getIndicatorDetails(indicatorIdList);

        List<List<IndicatorDto>> listOfIndicatorDtoList = new ArrayList<List<IndicatorDto>>();

        for (List<Integer> integers : listOfIndicatorIdList) {
            listOfIndicatorDtoList.add(
                    indicatorDtoList.stream()
                            .filter(x -> integers.contains(x.getIndicatorId()))
                            .collect(Collectors.toList())
            );
        }

        return listOfIndicatorDtoList;
    }
}
