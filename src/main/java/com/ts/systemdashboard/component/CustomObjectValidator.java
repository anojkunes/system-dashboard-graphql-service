package com.ts.systemdashboard.component;

import com.ts.systemdashboard.dto.view.error.ErrorDetailsDto;
import com.ts.systemdashboard.exception.DataValidationException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@Component
public class CustomObjectValidator {
    @Autowired
    private Validator bVal;

    @Autowired
    private MessageSource messageSource;

    public <T> void validateObjectList(@NonNull List<T> objectList, @NonNull Class<?>... validationGroups) throws DataValidationException {
        Assert.isTrue(!objectList.isEmpty(), "objectList cannot be empty");

        List<ErrorDetailsDto> errorDetails = new ArrayList<>();
        for (T object: objectList) {
            Set<ConstraintViolation<T>> violations = bVal.validate(object, validationGroups);
            if (!violations.isEmpty()) {
                errorDetails.add(this.getFieldErrors(violations, object));
            }
        }

        if(!errorDetails.isEmpty()) {
            throw new DataValidationException(errorDetails);
        }
    }

    private <T> ErrorDetailsDto getFieldErrors(Set<ConstraintViolation<T>> violations, T value) {
        List<String> errors = new ArrayList<>();
        ErrorDetailsDto errorDetail = new ErrorDetailsDto();
        for (ConstraintViolation<?> violation : violations) {
            try {
                final String code = this.getProperty(violation).replaceAll("\\[(.*?)\\]", "");
                log.info("value: {}", code);
                errors.add(
                        messageSource.getMessage(
                                code,
                                null,
                                null
                        )
                );
            } catch (Exception ex) {
                // default error message
                errors.add(
                        String.format("%s: %s", violation.getPropertyPath(), violation.getMessage())
                );
            }
        }

        errorDetail.setValue(value);
        errorDetail.setError(String.join(", ", errors));

        return errorDetail;
    }

    private String getProperty(final ConstraintViolation<?> violation) {
        final String fieldName = violation.getPropertyPath()
                .toString()
                .replaceAll("\\[(.*?)\\]", "");

        final String annotatedValidation = violation.getConstraintDescriptor()
                .getAnnotation()
                .annotationType()
                .getSimpleName();

        final String objectName = violation.getRootBean().getClass().getSimpleName();

        return objectName + "." + fieldName + "." + annotatedValidation;
    }
}
