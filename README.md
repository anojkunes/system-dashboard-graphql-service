# System Dashboard GraphQL Service
System dashboard service is a Java Spring boot project using [GraphQL SQPR Spring Boot Starter](https://github.com/leangen/graphql-spqr-spring-boot-starter) library. 
Run the Application and access the **GraphQL Playground** using [http://localhost:8045/system-dashboard-service/gui](http://localhost:8045/system-dashboard-service/gui) and provide the basic auth credentials which is found in the [properties](src/main/resources)

## GraphQL GUI Playground

Use these properties below to enable the graphql GUI
~~~
graphql.spqr.gui.enabled=true
graphql.spqr.gui.page-title=System Dashboard Service GraphQL Playground
graphql.spqr.gui.target-endpoint=/@project.packageName@/graphql
#connection model that provides a standard mechanism for slicing and paginating the result set
graphql.spqr.relay.connection-check-relaxed=true
~~~

Use the [payloads](src/main/input-payloads) specified for various Query and Mutation methods, and give Authorization Bearer Token to access the [http://localhost:8045/system-dashboard-service/graphql?access_token={token}](http://localhost:8045/system-dashboard-service/graphql?access_token={token}) endpoint
as shown below.

![GraphQLGui](src/main/screenshots/graphql-gui-playground.png?raw=true)
